// manalyzer.h

#ifndef MANALYZER_H
#define MANALYZER_H

#include <string>
#include <vector>
#include <deque>

#include "rootana_config.h"

#ifdef HAVE_CXX11_THREADS
#include <thread>
#include <mutex>
#endif

#include "midasio.h"
#include "VirtualOdb.h"

class TARootHelper;
class TAMultithreadHelper;
class TAFlowEvent;

class TARunInfo
{
 public:
   int fRunNo;
   std::string fFileName;
   VirtualOdb* fOdb;
   TARootHelper* fRoot;
   TAMultithreadHelper* fMtInfo;
   std::vector<std::string> fArgs;
   
 public:
   TARunInfo(int runno, const char* filename, const std::vector<std::string>& args);
   ~TARunInfo();

 public:
   void AddToFlowQueue(TAFlowEvent*);
   TAFlowEvent* ReadFlowQueue();

 private:
   TARunInfo() {}; // hidden default constructor

 private:
   std::deque<TAFlowEvent*> fFlowQueue;
};

class TAFlowEvent
{
 public:
   TAFlowEvent* fNext;

 public:
   TAFlowEvent(TAFlowEvent*);
   virtual ~TAFlowEvent();

   template<class T> T* Find()
      {
         TAFlowEvent* f = this;
         while (f) {
            T *ptr = dynamic_cast<T*>(f);
            if (ptr) return ptr;
            f = f->fNext;
         }
         return NULL;
      }

 private:
   TAFlowEvent() {}; // hidden default constructor 
};

typedef int TAFlags;

#define TAFlag_OK          0
#define TAFlag_SKIP    (1<<0)
#define TAFlag_QUIT    (1<<1)
#define TAFlag_WRITE   (1<<2)
#define TAFlag_DISPLAY (1<<3)

class TARunObject
{
 public:
   TARunObject(TARunInfo* runinfo); // ctor
   virtual ~TARunObject() {}; // dtor

 public:
   virtual void BeginRun(TARunInfo* runinfo); // begin of run
   virtual void EndRun(TARunInfo* runinfo); // end of run
   virtual void NextSubrun(TARunInfo* runinfo); // next subrun file

   virtual void PauseRun(TARunInfo* runinfo); // pause of run (if online)
   virtual void ResumeRun(TARunInfo* runinfo); // resume of run (if online)

   virtual void PreEndRun(TARunInfo* runinfo); // generate flow events before end of run

   virtual TAFlowEvent* Analyze(TARunInfo* runinfo, TMEvent* event, TAFlags* flags, TAFlowEvent* flow);
   virtual TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow);
   virtual void AnalyzeSpecialEvent(TARunInfo* runinfo, TMEvent* event);

 private:
   TARunObject(); // hidden default constructor
};

class TAFactory
{
 public:
   TAFactory() {}; // ctor
   virtual ~TAFactory() {}; // dtor

 public:
   virtual TARunObject* NewRunObject(TARunInfo* runinfo) = 0; // factory for Run objects

 public:
   virtual void Usage(); // Display usage (flags to pass to init etc)
   virtual void Init(const std::vector<std::string> &args); // start of analysis
   virtual void Finish(); // end of analysis
};

template<class T> class TAFactoryTemplate: public TAFactory
{
   T* NewRunObject(TARunInfo* runinfo)
   {
      return new T(runinfo);
   }
};

class TARegister
{
 public:
   TARegister(TAFactory* m);
   //static void Register(TAModuleInterface* m);
   //static std::vector<TAModuleInterface*>* fgModules;
   //static std::vector<TAModuleInterface*>* Get() const;
};

#ifdef HAVE_ROOT

#include "TFile.h"
#include "TDirectory.h"
#include "TApplication.h"

class XmlServer;
class THttpServer;

class TARootHelper
{
 public:
   static std::string fOutputFileName;
   TFile* fOutputFile;
   static TDirectory*   fgDir;
   static TApplication* fgApp;
   static XmlServer*    fgXmlServer;
   static THttpServer*  fgHttpServer;

 public:
   TARootHelper(const TARunInfo*);
   ~TARootHelper(); // dtor

 private:
   TARootHelper() { }; // hidden default constructor
};
#endif

#ifdef HAVE_CXX11_THREADS

typedef std::deque<TAFlowEvent*> TAFlowEventQueue;
typedef std::deque<TAFlags*>     TAFlagsQueue;

class TAMultithreadHelper
{
public:
   // per-module queues
   std::vector<TAFlowEventQueue> fMtFlowQueue;
   std::vector<TAFlagsQueue>     fMtFlagQueue;
   // lock for the queues
   std::vector<std::mutex>       fMtFlowQueueMutex;
   // per-module threads
   std::vector<std::thread*> fMtThreads;
   // maximum number of flow events to queue
   int fMtQueueDepth;
   // end-of-run event marker
   TAFlowEvent* fMtLastItemInQueue;
   // flag to shutdown all threads
   bool fMtShutdown;
   // quit flag from AnalyzeFlowEvent()
   bool fMtQuit;
   // queue settings
   int  fMtQueueFullUSleepTime;  // u seconds
   int  fMtQueueEmptyUSleepTime; // u seconds
   
   static bool gfMultithread;
   static int  gfMtMaxBacklog;
   static std::mutex gfLock; //Lock for modules to execute code that is not thread safe (many root fitting libraries)

public:
   TAMultithreadHelper(); // ctor
   ~TAMultithreadHelper(); // dtor
};
#endif


int manalyzer_main(int argc, char* argv[]);

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

